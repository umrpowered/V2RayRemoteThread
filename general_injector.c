#include <windows.h>
#include <stdio.h>
#include <psapi.h>

int isTargetExe(DWORD processId, char targetProcessName[]) {
    char szProcessName[MAX_PATH] = "";
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ,
                                   FALSE, processId );
    if (hProcess == NULL) return 0;
    HMODULE hMod;
    DWORD cbNeeded;
    if (!EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded)) return 0;
    GetModuleBaseNameA(hProcess, hMod, szProcessName,
                      sizeof(szProcessName)/sizeof(szProcessName[0]) );
    //_tprintf( TEXT("%u\t%s\n"), processId, szProcessName);
    CloseHandle(hProcess);

    if (!strcmp(targetProcessName, szProcessName)) {
        return 1;
    }
    return 0;
}

void injectImposter(DWORD pid, char dllname[]) {
    HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION|PROCESS_VM_OPERATION| PROCESS_VM_WRITE|PROCESS_VM_READ,
            FALSE, pid);
    if (!hProcess) {
        fprintf(stderr, "OpenProcess() error: %u\n", GetLastError());
        return;
    }
    LPVOID pLoadLibrary = (LPVOID) GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");

    // We allocate a memory space in this process to store the name of the DLL that it will load
    LPVOID pMemory = (LPVOID) VirtualAllocEx(hProcess, NULL, strlen(dllname)+1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    if (!pMemory) {
        fprintf(stderr, "VirtualAllocEx() error: %u\n", GetLastError());
        CloseHandle(hProcess);
        return;
    }


    // We write in this memory space the name of the DLL to inject
    WriteProcessMemory(hProcess, (LPVOID) pMemory, dllname, strlen(dllname)+1, NULL);

    // And finally we create a thread running in the context of the target process
    // This thread will only execute a LoadLibrary instruction with the path of the DLL to inject as argument.
    HANDLE RemoteThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE) pLoadLibrary, (LPVOID) pMemory, NULL, NULL);
    CloseHandle(hProcess);
    fprintf(stderr, "CreateRemoteThread done. %u\n", GetLastError());
}

// https://learn.microsoft.com/en-us/windows/win32/secauthz/enabling-and-disabling-privileges-in-c--
BOOL SetPrivilege(
    HANDLE hToken,          // access token handle
    LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
    BOOL bEnablePrivilege   // to enable or disable privilege
    )
{
    TOKEN_PRIVILEGES tp;
    LUID luid;

    if ( !LookupPrivilegeValue(
            NULL,            // lookup privilege on local system
            lpszPrivilege,   // privilege to lookup
            &luid ) )        // receives LUID of privilege
    {
        printf("LookupPrivilegeValue error: %u\n", GetLastError() );
        return FALSE;
    }

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    // Enable the privilege or disable all privileges.

    if ( !AdjustTokenPrivileges(
           hToken,
           FALSE,
           &tp,
           sizeof(TOKEN_PRIVILEGES),
           (PTOKEN_PRIVILEGES) NULL,
           (PDWORD) NULL) )
    {
          printf("AdjustTokenPrivileges error: %u\n", GetLastError() );
          return FALSE;
    }

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)

    {
          printf("The token does not have the specified privilege. \n");
          return FALSE;
    }

    return TRUE;
}


DWORD aProcesses[8192];
int main(int argc, char** argv) {
    DWORD cbNeeded, cProcesses;
    if (argc != 3) {
        fprintf(stderr, "usage: %s target_exe_name dll_name_or_path\n", argv[0]);
        return 2;
    }

    HANDLE hToken;
    if (!OpenProcessToken(GetCurrentProcess(), TOKEN_WRITE, &hToken)) {
        fprintf(stderr, "ERROR: OpenProcessToken() failed!\n");
        return 1;
    }
    if (!SetPrivilege(hToken, SE_DEBUG_NAME, 1) ) {
        fprintf(stderr, "ERROR: SeDebugPrivilege failed!\n");
        return 1;
    }
    unsigned int i = 0;
    if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded) ) {
        fprintf(stderr, "ERROR: EnumProcesses() failed!\n");
        return 1;
    }
    cProcesses = cbNeeded / sizeof(DWORD);
    for (i=0;i<cProcesses;i++) {
        DWORD pid = aProcesses[i];
        if (pid == 0) continue;
        if (!isTargetExe(pid, argv[1])) continue;
        printf("Found %s at %u\n", argv[1], pid);
        printf("Press ENTER to inject...\n");
        system("pause");
        injectImposter(pid, argv[2]);
        return 0;
    }
    return 0;
}
