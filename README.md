# V2RayRemoteThread

Build Win32 V2Ray DLL, and create its remote thread in another process.

## Build

  1. Install MinGW-w64 
  2. `make`
  3. Build a modified version of V2Ray

## About V2Ray
  
  This repository comes with a simple patch that makes [V2Ray](https://github.com/v2fly/v2ray-core). Once you have Go, Git and MinGW-w64 ready:
```bash
git clone https://github.com/v2fly/v2ray-core --depth 1 -b v5.12.1
cd v2ray-core
git am ../patch/0001-build-V2Ray-as-win32-DLL.patch
git am ../patch/0002-add-justReadConfigJsonV5.patch
sh test_win64_build.sh
```
  The `v2ray.dll` exports `justReadConfigJson()` and `justReadConfigJsonV5()`, which take no arguments, then read V2Ray configuration from `config.json` in _current working directory_ .

  The `v2ray_starter.dll` has a `DllMain` that loads `v2ray.dll` and execute `justReadConfigJson` on `DLL_PROCESS_ATTACH`

## Usage of general injector
 
  `general_injector.exe target.exe payload.dll`

  `general_injector.exe` will create a remote thread in (one of the) `target.exe` process, which calls `LoadLibraryA` to load `payload.dll`.
  Make sure that `payload.dll` exists in the same directory with `target.exe`, or can be located by `LoadLibraryA`

## Example of V2Ray injection

  1. Copy-paste `v2ray_starter.dll` and `v2ray.dll` into target executable directory.
  2. Make (or copy-paste) a valid `config.json` in _current working directory_ of target process.
      * In most cases, the directory which target executable lies in *is* its current working directory.
      * [Process Explorer](https://learn.microsoft.com/en-us/sysinternals/downloads/process-explorer) can reveal current working directory of processes, if unsure or target process exited after injection.

```
general_injector.exe target.exe v2ray_starter.dll
```

## Standalone steamwebhelper.exe
  
  1. `make dummylibs`
  2. Copy `steamwebhelper.exe` (x64) to this directory.
  3. Run `steamwebhelper.exe`.`
