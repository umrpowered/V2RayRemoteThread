CC=x86_64-w64-mingw32-gcc-win32
CXX=x86_64-w64-mingw32-g++-win32

all: v2ray_starter general_injector

v2ray_starter : v2ray_starter.c
	$(CC) $(CFLAGS) v2ray_starter.c -o v2ray_starter.dll -shared

general_injector : general_injector.c
	$(CC) $(CFLAGS) general_injector.c -o general_injector.exe -lpsapi -lcmcfg32
	
sleeper : sleeper.c
	$(CC) $(CFLAGS) sleeper.c -o sleeper.exe

dummylibs: dummy_libcef.c dummy_SDL3.c
	$(CC) $(CFLAGS) dummy_libcef.c -o libcef.dll -shared
	$(CC) $(CFLAGS) dummy_SDL3.c -o SDL3.dll -shared

clean:
	$(RM) v2ray_starter.dll
	$(RM) general_injector.exe
