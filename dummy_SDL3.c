#include <windows.h>

#define ENTRY(name) \
__declspec(dllexport) void name() { while (1) { Sleep(30000); } }

__declspec(dllexport)
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

ENTRY(SDL_AddEventWatch)
ENTRY(SDL_AddGamepadMapping)
ENTRY(SDL_AddGamepadMappingsFromRW)
ENTRY(SDL_AddHintCallback)
ENTRY(SDL_AddTimer)
ENTRY(SDL_AtomicAdd)
ENTRY(SDL_AtomicCAS)
ENTRY(SDL_AtomicCASPtr)
ENTRY(SDL_AtomicGet)
ENTRY(SDL_AtomicGetPtr)
ENTRY(SDL_AtomicLock)
ENTRY(SDL_AtomicSet)
ENTRY(SDL_AtomicSetPtr)
ENTRY(SDL_AtomicTryLock)
ENTRY(SDL_AtomicUnlock)
ENTRY(SDL_AttachVirtualJoystick)
ENTRY(SDL_AttachVirtualJoystickEx)
ENTRY(SDL_BlitSurface)
ENTRY(SDL_BlitSurfaceScaled)
ENTRY(SDL_BlitSurfaceUnchecked)
ENTRY(SDL_BlitSurfaceUncheckedScaled)
ENTRY(SDL_CaptureMouse)
ENTRY(SDL_ClearAudioStream)
ENTRY(SDL_ClearComposition)
ENTRY(SDL_ClearError)
ENTRY(SDL_ClearHints)
ENTRY(SDL_ClearQueuedAudio)
ENTRY(SDL_CloseAudioDevice)
ENTRY(SDL_CloseGamepad)
ENTRY(SDL_CloseJoystick)
ENTRY(SDL_CloseSensor)
ENTRY(SDL_ComposeCustomBlendMode)
ENTRY(SDL_CondBroadcast)
ENTRY(SDL_CondSignal)
ENTRY(SDL_CondWait)
ENTRY(SDL_CondWaitTimeout)
ENTRY(SDL_ConvertAudioSamples)
ENTRY(SDL_ConvertEventToRenderCoordinates)
ENTRY(SDL_ConvertPixels)
ENTRY(SDL_ConvertSurface)
ENTRY(SDL_ConvertSurfaceFormat)
ENTRY(SDL_CreateAudioStream)
ENTRY(SDL_CreateColorCursor)
ENTRY(SDL_CreateCond)
ENTRY(SDL_CreateCursor)
ENTRY(SDL_CreateMutex)
ENTRY(SDL_CreatePalette)
ENTRY(SDL_CreatePixelFormat)
ENTRY(SDL_CreateRW)
ENTRY(SDL_CreateRenderer)
ENTRY(SDL_CreateSemaphore)
ENTRY(SDL_CreateShapedWindow)
ENTRY(SDL_CreateSoftwareRenderer)
ENTRY(SDL_CreateSurface)
ENTRY(SDL_CreateSurfaceFrom)
ENTRY(SDL_CreateSystemCursor)
ENTRY(SDL_CreateTexture)
ENTRY(SDL_CreateTextureFromSurface)
ENTRY(SDL_CreateThread)
ENTRY(SDL_CreateThreadWithStackSize)
ENTRY(SDL_CreateWindow)
ENTRY(SDL_CreateWindowAndRenderer)
ENTRY(SDL_CreateWindowFrom)
ENTRY(SDL_CursorVisible)
ENTRY(SDL_DXGIGetOutputInfo)
ENTRY(SDL_DYNAPI_entry)
ENTRY(SDL_DelEventWatch)
ENTRY(SDL_DelHintCallback)
ENTRY(SDL_Delay)
ENTRY(SDL_DelayNS)
ENTRY(SDL_DequeueAudio)
ENTRY(SDL_DestroyAudioStream)
ENTRY(SDL_DestroyCond)
ENTRY(SDL_DestroyCursor)
ENTRY(SDL_DestroyMutex)
ENTRY(SDL_DestroyPalette)
ENTRY(SDL_DestroyPixelFormat)
ENTRY(SDL_DestroyRW)
ENTRY(SDL_DestroyRenderer)
ENTRY(SDL_DestroySemaphore)
ENTRY(SDL_DestroySurface)
ENTRY(SDL_DestroyTexture)
ENTRY(SDL_DestroyWindow)
ENTRY(SDL_DetachThread)
ENTRY(SDL_DetachVirtualJoystick)
ENTRY(SDL_Direct3D9GetAdapterIndex)
ENTRY(SDL_DisableScreenSaver)
ENTRY(SDL_DuplicateSurface)
ENTRY(SDL_EGL_GetCurrentEGLConfig)
ENTRY(SDL_EGL_GetCurrentEGLDisplay)
ENTRY(SDL_EGL_GetProcAddress)
ENTRY(SDL_EGL_GetWindowEGLSurface)
ENTRY(SDL_EGL_SetEGLAttributeCallbacks)
ENTRY(SDL_EnableScreenSaver)
ENTRY(SDL_Error)
ENTRY(SDL_EventEnabled)
ENTRY(SDL_FillSurfaceRect)
ENTRY(SDL_FillSurfaceRects)
ENTRY(SDL_FilterEvents)
ENTRY(SDL_FlashWindow)
ENTRY(SDL_FlushAudioStream)
ENTRY(SDL_FlushEvent)
ENTRY(SDL_FlushEvents)
ENTRY(SDL_GL_BindTexture)
ENTRY(SDL_GL_CreateContext)
ENTRY(SDL_GL_DeleteContext)
ENTRY(SDL_GL_ExtensionSupported)
ENTRY(SDL_GL_GetAttribute)
ENTRY(SDL_GL_GetCurrentContext)
ENTRY(SDL_GL_GetCurrentWindow)
ENTRY(SDL_GL_GetProcAddress)
ENTRY(SDL_GL_GetSwapInterval)
ENTRY(SDL_GL_LoadLibrary)
ENTRY(SDL_GL_MakeCurrent)
ENTRY(SDL_GL_ResetAttributes)
ENTRY(SDL_GL_SetAttribute)
ENTRY(SDL_GL_SetSwapInterval)
ENTRY(SDL_GL_SwapWindow)
ENTRY(SDL_GL_UnbindTexture)
ENTRY(SDL_GL_UnloadLibrary)
ENTRY(SDL_GUIDFromString)
ENTRY(SDL_GUIDToString)
ENTRY(SDL_GamepadConnected)
ENTRY(SDL_GamepadEventsEnabled)
ENTRY(SDL_GamepadHasAxis)
ENTRY(SDL_GamepadHasButton)
ENTRY(SDL_GamepadHasLED)
ENTRY(SDL_GamepadHasRumble)
ENTRY(SDL_GamepadHasRumbleTriggers)
ENTRY(SDL_GamepadHasSensor)
ENTRY(SDL_GamepadSensorEnabled)
ENTRY(SDL_GetAssertionHandler)
ENTRY(SDL_GetAssertionReport)
ENTRY(SDL_GetAudioDeviceName)
ENTRY(SDL_GetAudioDeviceSpec)
ENTRY(SDL_GetAudioDeviceStatus)
ENTRY(SDL_GetAudioDriver)
ENTRY(SDL_GetAudioStreamAvailable)
ENTRY(SDL_GetAudioStreamData)
ENTRY(SDL_GetBasePath)
ENTRY(SDL_GetCPUCacheLineSize)
ENTRY(SDL_GetCPUCount)
ENTRY(SDL_GetClipboardText)
ENTRY(SDL_GetClosestFullscreenDisplayMode)
ENTRY(SDL_GetCurrentAudioDriver)
ENTRY(SDL_GetCurrentDisplayMode)
ENTRY(SDL_GetCurrentRenderOutputSize)
ENTRY(SDL_GetCurrentVideoDriver)
ENTRY(SDL_GetCursor)
ENTRY(SDL_GetDefaultAssertionHandler)
ENTRY(SDL_GetDefaultAudioInfo)
ENTRY(SDL_GetDefaultCursor)
ENTRY(SDL_GetDesktopDisplayMode)
ENTRY(SDL_GetDisplayBounds)
ENTRY(SDL_GetDisplayForPoint)
ENTRY(SDL_GetDisplayForRect)
ENTRY(SDL_GetDisplayForWindow)
ENTRY(SDL_GetDisplayName)
ENTRY(SDL_GetDisplayOrientation)
ENTRY(SDL_GetDisplayUsableBounds)
ENTRY(SDL_GetDisplays)
ENTRY(SDL_GetError)
ENTRY(SDL_GetErrorMsg)
ENTRY(SDL_GetEventFilter)
ENTRY(SDL_GetFullscreenDisplayModes)
ENTRY(SDL_GetGamepadAppleSFSymbolsNameForAxis)
ENTRY(SDL_GetGamepadAppleSFSymbolsNameForButton)
ENTRY(SDL_GetGamepadAxis)
ENTRY(SDL_GetGamepadAxisFromString)
ENTRY(SDL_GetGamepadBindForAxis)
ENTRY(SDL_GetGamepadBindForButton)
ENTRY(SDL_GetGamepadButton)
ENTRY(SDL_GetGamepadButtonFromString)
ENTRY(SDL_GetGamepadFirmwareVersion)
ENTRY(SDL_GetGamepadFromInstanceID)
ENTRY(SDL_GetGamepadFromPlayerIndex)
ENTRY(SDL_GetGamepadInstanceGUID)
ENTRY(SDL_GetGamepadInstanceMapping)
ENTRY(SDL_GetGamepadInstanceName)
ENTRY(SDL_GetGamepadInstancePath)
ENTRY(SDL_GetGamepadInstancePlayerIndex)
ENTRY(SDL_GetGamepadInstanceProduct)
ENTRY(SDL_GetGamepadInstanceProductVersion)
ENTRY(SDL_GetGamepadInstanceType)
ENTRY(SDL_GetGamepadInstanceVendor)
ENTRY(SDL_GetGamepadJoystick)
ENTRY(SDL_GetGamepadMapping)
ENTRY(SDL_GetGamepadMappingForGUID)
ENTRY(SDL_GetGamepadMappingForIndex)
ENTRY(SDL_GetGamepadName)
ENTRY(SDL_GetGamepadPath)
ENTRY(SDL_GetGamepadPlayerIndex)
ENTRY(SDL_GetGamepadProduct)
ENTRY(SDL_GetGamepadProductVersion)
ENTRY(SDL_GetGamepadSensorData)
ENTRY(SDL_GetGamepadSensorDataRate)
ENTRY(SDL_GetGamepadSerial)
ENTRY(SDL_GetGamepadStringForAxis)
ENTRY(SDL_GetGamepadStringForButton)
ENTRY(SDL_GetGamepadTouchpadFinger)
ENTRY(SDL_GetGamepadType)
ENTRY(SDL_GetGamepadVendor)
ENTRY(SDL_GetGamepads)
ENTRY(SDL_GetGlobalMouseState)
ENTRY(SDL_GetGrabbedWindow)
ENTRY(SDL_GetHint)
ENTRY(SDL_GetHintBoolean)
ENTRY(SDL_GetJoystickAxis)
ENTRY(SDL_GetJoystickAxisInitialState)
ENTRY(SDL_GetJoystickButton)
ENTRY(SDL_GetJoystickFirmwareVersion)
ENTRY(SDL_GetJoystickFromInstanceID)
ENTRY(SDL_GetJoystickFromPlayerIndex)
ENTRY(SDL_GetJoystickGUID)
ENTRY(SDL_GetJoystickGUIDFromString)
ENTRY(SDL_GetJoystickGUIDInfo)
ENTRY(SDL_GetJoystickGUIDString)
ENTRY(SDL_GetJoystickHat)
ENTRY(SDL_GetJoystickInstanceGUID)
ENTRY(SDL_GetJoystickInstanceID)
ENTRY(SDL_GetJoystickInstanceName)
ENTRY(SDL_GetJoystickInstancePath)
ENTRY(SDL_GetJoystickInstancePlayerIndex)
ENTRY(SDL_GetJoystickInstanceProduct)
ENTRY(SDL_GetJoystickInstanceProductVersion)
ENTRY(SDL_GetJoystickInstanceType)
ENTRY(SDL_GetJoystickInstanceVendor)
ENTRY(SDL_GetJoystickName)
ENTRY(SDL_GetJoystickPath)
ENTRY(SDL_GetJoystickPlayerIndex)
ENTRY(SDL_GetJoystickPowerLevel)
ENTRY(SDL_GetJoystickProduct)
ENTRY(SDL_GetJoystickProductVersion)
ENTRY(SDL_GetJoystickSerial)
ENTRY(SDL_GetJoystickType)
ENTRY(SDL_GetJoystickVendor)
ENTRY(SDL_GetJoysticks)
ENTRY(SDL_GetKeyFromName)
ENTRY(SDL_GetKeyFromScancode)
ENTRY(SDL_GetKeyName)
ENTRY(SDL_GetKeyboardFocus)
ENTRY(SDL_GetKeyboardState)
ENTRY(SDL_GetMasksForPixelFormatEnum)
ENTRY(SDL_GetMemoryFunctions)
ENTRY(SDL_GetModState)
ENTRY(SDL_GetMouseFocus)
ENTRY(SDL_GetMouseState)
ENTRY(SDL_GetNumAllocations)
ENTRY(SDL_GetNumAudioDevices)
ENTRY(SDL_GetNumAudioDrivers)
ENTRY(SDL_GetNumGamepadMappings)
ENTRY(SDL_GetNumGamepadTouchpadFingers)
ENTRY(SDL_GetNumGamepadTouchpads)
ENTRY(SDL_GetNumJoystickAxes)
ENTRY(SDL_GetNumJoystickButtons)
ENTRY(SDL_GetNumJoystickHats)
ENTRY(SDL_GetNumRenderDrivers)
ENTRY(SDL_GetNumTouchDevices)
ENTRY(SDL_GetNumTouchFingers)
ENTRY(SDL_GetNumVideoDrivers)
ENTRY(SDL_GetOriginalMemoryFunctions)
ENTRY(SDL_GetPerformanceCounter)
ENTRY(SDL_GetPerformanceFrequency)
ENTRY(SDL_GetPixelFormatEnumForMasks)
ENTRY(SDL_GetPixelFormatName)
ENTRY(SDL_GetPlatform)
ENTRY(SDL_GetPowerInfo)
ENTRY(SDL_GetPrefPath)
ENTRY(SDL_GetPreferredLocales)
ENTRY(SDL_GetPrimaryDisplay)
ENTRY(SDL_GetPrimarySelectionText)
ENTRY(SDL_GetQueuedAudioSize)
ENTRY(SDL_GetRGB)
ENTRY(SDL_GetRGBA)
ENTRY(SDL_GetRectAndLineIntersection)
ENTRY(SDL_GetRectAndLineIntersectionFloat)
ENTRY(SDL_GetRectEnclosingPoints)
ENTRY(SDL_GetRectEnclosingPointsFloat)
ENTRY(SDL_GetRectIntersection)
ENTRY(SDL_GetRectIntersectionFloat)
ENTRY(SDL_GetRectUnion)
ENTRY(SDL_GetRectUnionFloat)
ENTRY(SDL_GetRelativeMouseMode)
ENTRY(SDL_GetRelativeMouseState)
ENTRY(SDL_GetRenderClipRect)
ENTRY(SDL_GetRenderD3D11Device)
ENTRY(SDL_GetRenderD3D9Device)
ENTRY(SDL_GetRenderDrawBlendMode)
ENTRY(SDL_GetRenderDrawColor)
ENTRY(SDL_GetRenderDriver)
ENTRY(SDL_GetRenderLogicalPresentation)
ENTRY(SDL_GetRenderMetalCommandEncoder)
ENTRY(SDL_GetRenderMetalLayer)
ENTRY(SDL_GetRenderOutputSize)
ENTRY(SDL_GetRenderScale)
ENTRY(SDL_GetRenderTarget)
ENTRY(SDL_GetRenderVSync)
ENTRY(SDL_GetRenderViewport)
ENTRY(SDL_GetRenderWindow)
ENTRY(SDL_GetRenderWindowSize)
ENTRY(SDL_GetRenderer)
ENTRY(SDL_GetRendererInfo)
ENTRY(SDL_GetRevision)
ENTRY(SDL_GetScancodeFromKey)
ENTRY(SDL_GetScancodeFromName)
ENTRY(SDL_GetScancodeName)
ENTRY(SDL_GetSensorData)
ENTRY(SDL_GetSensorFromInstanceID)
ENTRY(SDL_GetSensorInstanceID)
ENTRY(SDL_GetSensorInstanceName)
ENTRY(SDL_GetSensorInstanceNonPortableType)
ENTRY(SDL_GetSensorInstanceType)
ENTRY(SDL_GetSensorName)
ENTRY(SDL_GetSensorNonPortableType)
ENTRY(SDL_GetSensorType)
ENTRY(SDL_GetSensors)
ENTRY(SDL_GetShapedWindowMode)
ENTRY(SDL_GetSurfaceAlphaMod)
ENTRY(SDL_GetSurfaceBlendMode)
ENTRY(SDL_GetSurfaceClipRect)
ENTRY(SDL_GetSurfaceColorKey)
ENTRY(SDL_GetSurfaceColorMod)
ENTRY(SDL_GetSystemRAM)
ENTRY(SDL_GetTextureAlphaMod)
ENTRY(SDL_GetTextureBlendMode)
ENTRY(SDL_GetTextureColorMod)
ENTRY(SDL_GetTextureScaleMode)
ENTRY(SDL_GetTextureUserData)
ENTRY(SDL_GetThreadID)
ENTRY(SDL_GetThreadName)
ENTRY(SDL_GetTicks)
ENTRY(SDL_GetTicksNS)
ENTRY(SDL_GetTouchDevice)
ENTRY(SDL_GetTouchDeviceType)
ENTRY(SDL_GetTouchFinger)
ENTRY(SDL_GetTouchName)
ENTRY(SDL_GetVersion)
ENTRY(SDL_GetVideoDriver)
ENTRY(SDL_GetWindowBordersSize)
ENTRY(SDL_GetWindowData)
ENTRY(SDL_GetWindowFlags)
ENTRY(SDL_GetWindowFromID)
ENTRY(SDL_GetWindowFullscreenMode)
ENTRY(SDL_GetWindowGrab)
ENTRY(SDL_GetWindowICCProfile)
ENTRY(SDL_GetWindowID)
ENTRY(SDL_GetWindowKeyboardGrab)
ENTRY(SDL_GetWindowMaximumSize)
ENTRY(SDL_GetWindowMinimumSize)
ENTRY(SDL_GetWindowMouseGrab)
ENTRY(SDL_GetWindowMouseRect)
ENTRY(SDL_GetWindowOpacity)
ENTRY(SDL_GetWindowPixelFormat)
ENTRY(SDL_GetWindowPosition)
ENTRY(SDL_GetWindowSize)
ENTRY(SDL_GetWindowSizeInPixels)
ENTRY(SDL_GetWindowSurface)
ENTRY(SDL_GetWindowTitle)
ENTRY(SDL_GetWindowWMInfo)
ENTRY(SDL_GetYUVConversionMode)
ENTRY(SDL_GetYUVConversionModeForResolution)
ENTRY(SDL_HapticClose)
ENTRY(SDL_HapticDestroyEffect)
ENTRY(SDL_HapticEffectSupported)
ENTRY(SDL_HapticGetEffectStatus)
ENTRY(SDL_HapticIndex)
ENTRY(SDL_HapticName)
ENTRY(SDL_HapticNewEffect)
ENTRY(SDL_HapticNumAxes)
ENTRY(SDL_HapticNumEffects)
ENTRY(SDL_HapticNumEffectsPlaying)
ENTRY(SDL_HapticOpen)
ENTRY(SDL_HapticOpenFromJoystick)
ENTRY(SDL_HapticOpenFromMouse)
ENTRY(SDL_HapticOpened)
ENTRY(SDL_HapticPause)
ENTRY(SDL_HapticQuery)
ENTRY(SDL_HapticRumbleInit)
ENTRY(SDL_HapticRumblePlay)
ENTRY(SDL_HapticRumbleStop)
ENTRY(SDL_HapticRumbleSupported)
ENTRY(SDL_HapticRunEffect)
ENTRY(SDL_HapticSetAutocenter)
ENTRY(SDL_HapticSetGain)
ENTRY(SDL_HapticStopAll)
ENTRY(SDL_HapticStopEffect)
ENTRY(SDL_HapticUnpause)
ENTRY(SDL_HapticUpdateEffect)
ENTRY(SDL_HasARMSIMD)
ENTRY(SDL_HasAVX)
ENTRY(SDL_HasAVX2)
ENTRY(SDL_HasAVX512F)
ENTRY(SDL_HasAltiVec)
ENTRY(SDL_HasClipboardText)
ENTRY(SDL_HasEvent)
ENTRY(SDL_HasEvents)
ENTRY(SDL_HasLASX)
ENTRY(SDL_HasLSX)
ENTRY(SDL_HasMMX)
ENTRY(SDL_HasNEON)
ENTRY(SDL_HasPrimarySelectionText)
ENTRY(SDL_HasRDTSC)
ENTRY(SDL_HasRectIntersection)
ENTRY(SDL_HasRectIntersectionFloat)
ENTRY(SDL_HasSSE)
ENTRY(SDL_HasSSE2)
ENTRY(SDL_HasSSE3)
ENTRY(SDL_HasSSE41)
ENTRY(SDL_HasSSE42)
ENTRY(SDL_HasScreenKeyboardSupport)
ENTRY(SDL_HideCursor)
ENTRY(SDL_HideWindow)
ENTRY(SDL_Init)
ENTRY(SDL_InitSubSystem)
ENTRY(SDL_IsGamepad)
ENTRY(SDL_IsJoystickVirtual)
ENTRY(SDL_IsShapedWindow)
ENTRY(SDL_IsTablet)
ENTRY(SDL_JoystickConnected)
ENTRY(SDL_JoystickEventsEnabled)
ENTRY(SDL_JoystickHasLED)
ENTRY(SDL_JoystickHasRumble)
ENTRY(SDL_JoystickHasRumbleTriggers)
ENTRY(SDL_JoystickIsHaptic)
ENTRY(SDL_LoadBMP_RW)
ENTRY(SDL_LoadFile)
ENTRY(SDL_LoadFile_RW)
ENTRY(SDL_LoadFunction)
ENTRY(SDL_LoadObject)
ENTRY(SDL_LoadWAV_RW)
ENTRY(SDL_LockAudioDevice)
ENTRY(SDL_LockJoysticks)
ENTRY(SDL_LockMutex)
ENTRY(SDL_LockSurface)
ENTRY(SDL_LockTexture)
ENTRY(SDL_LockTextureToSurface)
ENTRY(SDL_Log)
ENTRY(SDL_LogCritical)
ENTRY(SDL_LogDebug)
ENTRY(SDL_LogError)
ENTRY(SDL_LogGetOutputFunction)
ENTRY(SDL_LogGetPriority)
ENTRY(SDL_LogInfo)
ENTRY(SDL_LogMessage)
ENTRY(SDL_LogMessageV)
ENTRY(SDL_LogResetPriorities)
ENTRY(SDL_LogSetAllPriority)
ENTRY(SDL_LogSetOutputFunction)
ENTRY(SDL_LogSetPriority)
ENTRY(SDL_LogVerbose)
ENTRY(SDL_LogWarn)
ENTRY(SDL_MapRGB)
ENTRY(SDL_MapRGBA)
ENTRY(SDL_MaximizeWindow)
ENTRY(SDL_MemoryBarrierAcquireFunction)
ENTRY(SDL_MemoryBarrierReleaseFunction)
ENTRY(SDL_Metal_CreateView)
ENTRY(SDL_Metal_DestroyView)
ENTRY(SDL_Metal_GetLayer)
ENTRY(SDL_MinimizeWindow)
ENTRY(SDL_MixAudioFormat)
ENTRY(SDL_MouseIsHaptic)
ENTRY(SDL_NumHaptics)
ENTRY(SDL_OnApplicationDidBecomeActive)
ENTRY(SDL_OnApplicationDidEnterBackground)
ENTRY(SDL_OnApplicationDidReceiveMemoryWarning)
ENTRY(SDL_OnApplicationWillEnterForeground)
ENTRY(SDL_OnApplicationWillResignActive)
ENTRY(SDL_OnApplicationWillTerminate)
ENTRY(SDL_OpenAudioDevice)
ENTRY(SDL_OpenGamepad)
ENTRY(SDL_OpenJoystick)
ENTRY(SDL_OpenSensor)
ENTRY(SDL_OpenURL)
ENTRY(SDL_PauseAudioDevice)
ENTRY(SDL_PeepEvents)
ENTRY(SDL_PlayAudioDevice)
ENTRY(SDL_PollEvent)
ENTRY(SDL_PremultiplyAlpha)
ENTRY(SDL_PumpEvents)
ENTRY(SDL_PushEvent)
ENTRY(SDL_PutAudioStreamData)
ENTRY(SDL_QueryTexture)
ENTRY(SDL_QueueAudio)
ENTRY(SDL_Quit)
ENTRY(SDL_QuitSubSystem)
ENTRY(SDL_RWFromConstMem)
ENTRY(SDL_RWFromFile)
ENTRY(SDL_RWFromMem)
ENTRY(SDL_RWclose)
ENTRY(SDL_RWread)
ENTRY(SDL_RWseek)
ENTRY(SDL_RWsize)
ENTRY(SDL_RWtell)
ENTRY(SDL_RWwrite)
ENTRY(SDL_RaiseWindow)
ENTRY(SDL_ReadBE16)
ENTRY(SDL_ReadBE32)
ENTRY(SDL_ReadBE64)
ENTRY(SDL_ReadLE16)
ENTRY(SDL_ReadLE32)
ENTRY(SDL_ReadLE64)
ENTRY(SDL_ReadU8)
ENTRY(SDL_RegisterApp)
ENTRY(SDL_RegisterEvents)
ENTRY(SDL_RemoveTimer)
ENTRY(SDL_RenderClear)
ENTRY(SDL_RenderClipEnabled)
ENTRY(SDL_RenderCoordinatesFromWindow)
ENTRY(SDL_RenderCoordinatesToWindow)
ENTRY(SDL_RenderFillRect)
ENTRY(SDL_RenderFillRects)
ENTRY(SDL_RenderFlush)
ENTRY(SDL_RenderGeometry)
ENTRY(SDL_RenderGeometryRaw)
ENTRY(SDL_RenderGetD3D12Device)
ENTRY(SDL_RenderLine)
ENTRY(SDL_RenderLines)
ENTRY(SDL_RenderPoint)
ENTRY(SDL_RenderPoints)
ENTRY(SDL_RenderPresent)
ENTRY(SDL_RenderReadPixels)
ENTRY(SDL_RenderRect)
ENTRY(SDL_RenderRects)
ENTRY(SDL_RenderTexture)
ENTRY(SDL_RenderTextureRotated)
ENTRY(SDL_ReportAssertion)
ENTRY(SDL_ResetAssertionReport)
ENTRY(SDL_ResetHint)
ENTRY(SDL_ResetHints)
ENTRY(SDL_ResetKeyboard)
ENTRY(SDL_RestoreWindow)
ENTRY(SDL_RumbleGamepad)
ENTRY(SDL_RumbleGamepadTriggers)
ENTRY(SDL_RumbleJoystick)
ENTRY(SDL_RumbleJoystickTriggers)
ENTRY(SDL_RunApp)
ENTRY(SDL_SIMDGetAlignment)
ENTRY(SDL_SaveBMP_RW)
ENTRY(SDL_ScreenKeyboardShown)
ENTRY(SDL_ScreenSaverEnabled)
ENTRY(SDL_SemPost)
ENTRY(SDL_SemTryWait)
ENTRY(SDL_SemValue)
ENTRY(SDL_SemWait)
ENTRY(SDL_SemWaitTimeout)
ENTRY(SDL_SendGamepadEffect)
ENTRY(SDL_SendJoystickEffect)
ENTRY(SDL_SetAssertionHandler)
ENTRY(SDL_SetClipboardText)
ENTRY(SDL_SetCursor)
ENTRY(SDL_SetError)
ENTRY(SDL_SetEventEnabled)
ENTRY(SDL_SetEventFilter)
ENTRY(SDL_SetGamepadEventsEnabled)
ENTRY(SDL_SetGamepadLED)
ENTRY(SDL_SetGamepadPlayerIndex)
ENTRY(SDL_SetGamepadSensorEnabled)
ENTRY(SDL_SetHint)
ENTRY(SDL_SetHintWithPriority)
ENTRY(SDL_SetJoystickEventsEnabled)
ENTRY(SDL_SetJoystickLED)
ENTRY(SDL_SetJoystickPlayerIndex)
ENTRY(SDL_SetJoystickVirtualAxis)
ENTRY(SDL_SetJoystickVirtualButton)
ENTRY(SDL_SetJoystickVirtualHat)
ENTRY(SDL_SetMainReady)
ENTRY(SDL_SetMemoryFunctions)
ENTRY(SDL_SetModState)
ENTRY(SDL_SetPaletteColors)
ENTRY(SDL_SetPixelFormatPalette)
ENTRY(SDL_SetPrimarySelectionText)
ENTRY(SDL_SetRelativeMouseMode)
ENTRY(SDL_SetRenderClipRect)
ENTRY(SDL_SetRenderDrawBlendMode)
ENTRY(SDL_SetRenderDrawColor)
ENTRY(SDL_SetRenderLogicalPresentation)
ENTRY(SDL_SetRenderScale)
ENTRY(SDL_SetRenderTarget)
ENTRY(SDL_SetRenderVSync)
ENTRY(SDL_SetRenderViewport)
ENTRY(SDL_SetSurfaceAlphaMod)
ENTRY(SDL_SetSurfaceBlendMode)
ENTRY(SDL_SetSurfaceClipRect)
ENTRY(SDL_SetSurfaceColorKey)
ENTRY(SDL_SetSurfaceColorMod)
ENTRY(SDL_SetSurfacePalette)
ENTRY(SDL_SetSurfaceRLE)
ENTRY(SDL_SetTextInputRect)
ENTRY(SDL_SetTextureAlphaMod)
ENTRY(SDL_SetTextureBlendMode)
ENTRY(SDL_SetTextureColorMod)
ENTRY(SDL_SetTextureScaleMode)
ENTRY(SDL_SetTextureUserData)
ENTRY(SDL_SetThreadPriority)
ENTRY(SDL_SetWindowAlwaysOnTop)
ENTRY(SDL_SetWindowBordered)
ENTRY(SDL_SetWindowData)
ENTRY(SDL_SetWindowFullscreen)
ENTRY(SDL_SetWindowFullscreenMode)
ENTRY(SDL_SetWindowGrab)
ENTRY(SDL_SetWindowHitTest)
ENTRY(SDL_SetWindowIcon)
ENTRY(SDL_SetWindowInputFocus)
ENTRY(SDL_SetWindowKeyboardGrab)
ENTRY(SDL_SetWindowMaximumSize)
ENTRY(SDL_SetWindowMinimumSize)
ENTRY(SDL_SetWindowModalFor)
ENTRY(SDL_SetWindowMouseGrab)
ENTRY(SDL_SetWindowMouseRect)
ENTRY(SDL_SetWindowOpacity)
ENTRY(SDL_SetWindowPosition)
ENTRY(SDL_SetWindowResizable)
ENTRY(SDL_SetWindowShape)
ENTRY(SDL_SetWindowSize)
ENTRY(SDL_SetWindowTitle)
ENTRY(SDL_SetWindowsMessageHook)
ENTRY(SDL_SetYUVConversionMode)
ENTRY(SDL_ShowCursor)
ENTRY(SDL_ShowMessageBox)
ENTRY(SDL_ShowSimpleMessageBox)
ENTRY(SDL_ShowWindow)
ENTRY(SDL_SoftStretch)
ENTRY(SDL_SoftStretchLinear)
ENTRY(SDL_StartTextInput)
ENTRY(SDL_StopTextInput)
ENTRY(SDL_SurfaceHasColorKey)
ENTRY(SDL_SurfaceHasRLE)
ENTRY(SDL_TLSCleanup)
ENTRY(SDL_TLSCreate)
ENTRY(SDL_TLSGet)
ENTRY(SDL_TLSSet)
ENTRY(SDL_TextInputActive)
ENTRY(SDL_TextInputShown)
ENTRY(SDL_ThreadID)
ENTRY(SDL_TryLockMutex)
ENTRY(SDL_UnloadObject)
ENTRY(SDL_UnlockAudioDevice)
ENTRY(SDL_UnlockJoysticks)
ENTRY(SDL_UnlockMutex)
ENTRY(SDL_UnlockSurface)
ENTRY(SDL_UnlockTexture)
ENTRY(SDL_UnregisterApp)
ENTRY(SDL_UpdateGamepads)
ENTRY(SDL_UpdateJoysticks)
ENTRY(SDL_UpdateNVTexture)
ENTRY(SDL_UpdateSensors)
ENTRY(SDL_UpdateTexture)
ENTRY(SDL_UpdateWindowSurface)
ENTRY(SDL_UpdateWindowSurfaceRects)
ENTRY(SDL_UpdateYUVTexture)
ENTRY(SDL_Vulkan_CreateSurface)
ENTRY(SDL_Vulkan_GetInstanceExtensions)
ENTRY(SDL_Vulkan_GetVkGetInstanceProcAddr)
ENTRY(SDL_Vulkan_LoadLibrary)
ENTRY(SDL_Vulkan_UnloadLibrary)
ENTRY(SDL_WaitEvent)
ENTRY(SDL_WaitEventTimeout)
ENTRY(SDL_WaitThread)
ENTRY(SDL_WarpMouseGlobal)
ENTRY(SDL_WarpMouseInWindow)
ENTRY(SDL_WasInit)
ENTRY(SDL_WriteBE16)
ENTRY(SDL_WriteBE32)
ENTRY(SDL_WriteBE64)
ENTRY(SDL_WriteLE16)
ENTRY(SDL_WriteLE32)
ENTRY(SDL_WriteLE64)
ENTRY(SDL_WriteU8)
ENTRY(SDL_abs)
ENTRY(SDL_acos)
ENTRY(SDL_acosf)
ENTRY(SDL_aligned_alloc)
ENTRY(SDL_aligned_free)
ENTRY(SDL_asin)
ENTRY(SDL_asinf)
ENTRY(SDL_asprintf)
ENTRY(SDL_atan)
ENTRY(SDL_atan2)
ENTRY(SDL_atan2f)
ENTRY(SDL_atanf)
ENTRY(SDL_atof)
ENTRY(SDL_atoi)
ENTRY(SDL_bsearch)
ENTRY(SDL_calloc)
ENTRY(SDL_ceil)
ENTRY(SDL_ceilf)
ENTRY(SDL_copysign)
ENTRY(SDL_copysignf)
ENTRY(SDL_cos)
ENTRY(SDL_cosf)
ENTRY(SDL_crc16)
ENTRY(SDL_crc32)
ENTRY(SDL_exp)
ENTRY(SDL_expf)
ENTRY(SDL_fabs)
ENTRY(SDL_fabsf)
ENTRY(SDL_floor)
ENTRY(SDL_floorf)
ENTRY(SDL_fmod)
ENTRY(SDL_fmodf)
ENTRY(SDL_free)
ENTRY(SDL_getenv)
ENTRY(SDL_hid_ble_scan)
ENTRY(SDL_hid_close)
ENTRY(SDL_hid_device_change_count)
ENTRY(SDL_hid_enumerate)
ENTRY(SDL_hid_exit)
ENTRY(SDL_hid_free_enumeration)
ENTRY(SDL_hid_get_feature_report)
ENTRY(SDL_hid_get_indexed_string)
ENTRY(SDL_hid_get_manufacturer_string)
ENTRY(SDL_hid_get_product_string)
ENTRY(SDL_hid_get_serial_number_string)
ENTRY(SDL_hid_init)
ENTRY(SDL_hid_open)
ENTRY(SDL_hid_open_path)
ENTRY(SDL_hid_read)
ENTRY(SDL_hid_read_timeout)
ENTRY(SDL_hid_send_feature_report)
ENTRY(SDL_hid_set_nonblocking)
ENTRY(SDL_hid_write)
ENTRY(SDL_iconv)
ENTRY(SDL_iconv_close)
ENTRY(SDL_iconv_open)
ENTRY(SDL_iconv_string)
ENTRY(SDL_isalnum)
ENTRY(SDL_isalpha)
ENTRY(SDL_isblank)
ENTRY(SDL_iscntrl)
ENTRY(SDL_isdigit)
ENTRY(SDL_isgraph)
ENTRY(SDL_islower)
ENTRY(SDL_isprint)
ENTRY(SDL_ispunct)
ENTRY(SDL_isspace)
ENTRY(SDL_isupper)
ENTRY(SDL_isxdigit)
ENTRY(SDL_itoa)
ENTRY(SDL_lltoa)
ENTRY(SDL_log)
ENTRY(SDL_log10)
ENTRY(SDL_log10f)
ENTRY(SDL_logf)
ENTRY(SDL_lround)
ENTRY(SDL_lroundf)
ENTRY(SDL_ltoa)
ENTRY(SDL_malloc)
ENTRY(SDL_memcmp)
ENTRY(SDL_memcpy)
ENTRY(SDL_memmove)
ENTRY(SDL_memset)
ENTRY(SDL_memset4)
ENTRY(SDL_modf)
ENTRY(SDL_modff)
ENTRY(SDL_pow)
ENTRY(SDL_powf)
ENTRY(SDL_qsort)
ENTRY(SDL_realloc)
ENTRY(SDL_round)
ENTRY(SDL_roundf)
ENTRY(SDL_scalbn)
ENTRY(SDL_scalbnf)
ENTRY(SDL_setenv)
ENTRY(SDL_sin)
ENTRY(SDL_sinf)
ENTRY(SDL_snprintf)
ENTRY(SDL_sqrt)
ENTRY(SDL_sqrtf)
ENTRY(SDL_sscanf)
ENTRY(SDL_strcasecmp)
ENTRY(SDL_strcasestr)
ENTRY(SDL_strchr)
ENTRY(SDL_strcmp)
ENTRY(SDL_strdup)
ENTRY(SDL_strlcat)
ENTRY(SDL_strlcpy)
ENTRY(SDL_strlen)
ENTRY(SDL_strlwr)
ENTRY(SDL_strncasecmp)
ENTRY(SDL_strncmp)
ENTRY(SDL_strrchr)
ENTRY(SDL_strrev)
ENTRY(SDL_strstr)
ENTRY(SDL_strtod)
ENTRY(SDL_strtokr)
ENTRY(SDL_strtol)
ENTRY(SDL_strtoll)
ENTRY(SDL_strtoul)
ENTRY(SDL_strtoull)
ENTRY(SDL_strupr)
ENTRY(SDL_tan)
ENTRY(SDL_tanf)
ENTRY(SDL_tolower)
ENTRY(SDL_toupper)
ENTRY(SDL_trunc)
ENTRY(SDL_truncf)
ENTRY(SDL_uitoa)
ENTRY(SDL_ulltoa)
ENTRY(SDL_ultoa)
ENTRY(SDL_utf8strlcpy)
ENTRY(SDL_utf8strlen)
ENTRY(SDL_utf8strnlen)
ENTRY(SDL_vasprintf)
ENTRY(SDL_vsnprintf)
ENTRY(SDL_vsscanf)
ENTRY(SDL_wcscasecmp)
ENTRY(SDL_wcscmp)
ENTRY(SDL_wcsdup)
ENTRY(SDL_wcslcat)
ENTRY(SDL_wcslcpy)
ENTRY(SDL_wcslen)
ENTRY(SDL_wcsncasecmp)
ENTRY(SDL_wcsncmp)
ENTRY(SDL_wcsstr)
