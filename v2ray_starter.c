#include <windows.h>
#include <psapi.h>
#include <tchar.h>
typedef int(*pfn_entry)();

__declspec(dllexport)
DWORD WINAPI NakadashiImposter(LPVOID lpThreadParameter) {
    HMODULE hLib = LoadLibraryA("v2ray.dll");
    if (!hLib) {
        ExitProcess(1);
        return 1;
    }
    // You can also use "justReadConfigJson" if you prefer JSON format
    pfn_entry entry = (pfn_entry)GetProcAddress(hLib, "justReadConfigJsonV5");
    int code = entry();
    ExitProcess(code);
    return 0;
}

__declspec(dllexport)
void doSomething() {
    HANDLE hThread = CreateThread(NULL, NULL, NakadashiImposter, NULL, NULL, NULL);
    if (hThread) {
        CloseHandle(hThread);
    }
}

__declspec(dllexport)
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        //MessageBox(NULL, "DLL_PROCESS_ATTACH", "Launching polipo...", NULL);
        doSomething();
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

